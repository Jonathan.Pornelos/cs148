<!--file that displays the list boxes of the current classes-->
<?php
include 'top.php';
include 'constants.php';
?>

    <h1>Home</h1>
    
    <!--Create a form of the data passed in about the current classes-->
    <form action= '<?php print $phpSelf;?>' 
          method="post" id="frmRegister">
   
        <!--include data file that actually makes the array of current classes-->
        <?php include('data.php'); ?>
        
    </form>
    
        <?php 

    $file = fopen($filename, 'r');

    /* This reads the first row, which in our case is the column headers */
    $headers = fgetcsv($file);

    /* the while (similar to a for loop) loop keeps executing until we reach 
     * the end of the file at which point it stops. the resulting variable 
     * $records is an array with all our data. */

    while (!feof($file)) {
        $selectedRecords[] = fgetcsv($file);
    }

//closes the file
    fclose($file);
    
//    Creates a table of all of the data of the class that is selected with headers
    print'<table>';
    foreach ($headers as $oneHeader) {
        print"<th>$oneHeader</th>";
        print "\n";
        }
 
     foreach ($selectedRecords as $selectedTableRec) {
        if ($selectedTableRec[3] == $class And $selectedTableRec!='') {
            print"\n<tr>";
            print '<option>' . '<span class="subj">' . "<td>$selectedTableRec[0]</td>". '</span>' . '&nbsp;'; //class subject
            print '<span class="classNum">' ."<td>$selectedTableRec[1]</td>". '</span>' . '&nbsp;'; //class number
            print '<span class="title">' . "<td>$selectedTableRec[2]</td>" . '</span>'; //class title
            print '<span class="compNum">' . "<td>$selectedTableRec[3]</td>" . '</span>'; //class comp number
            print '<span class="section">' . "<td>$selectedTableRec[4]</td>". '</span>'. '</option>'; //class section
            print '<span class="lecLab">' ."<td>$selectedTableRec[5]</td>". '</span>'; //class lecture lab
            print '<span class="campCode">' . "<td>$selectedTableRec[6]</td>" . '</span>'; //campus code
            print '<span class="maxEnroll">' . "<td>$selectedTableRec[7]</td>" . '</span>'; //max enrollment limit
            print '<span class="curEnroll">' . "<td>$selectedTableRec[8]</td>". '</span>'; //current amount enrolled in the class
            print '<span class="startTime">' ."<td>$selectedTableRec[9]</td>". '</span>'; //start time of the class
            print '<span class="endTime">' . "<td>$selectedTableRec[10]</td>" . '</span>'; //end time of the class
            print '<span class="days">' . "<td>$selectedTableRec[11]</td>" . '</span>'; //what days the class takes place
            print '<span class="credits">' . "<td>$selectedTableRec[12]</td>". '</span>';//how many credits the course is
            print '<span class="building">' ."<td>$selectedTableRec[13]</td>". '</span>';//what building the class takes place
            print '<span class="room">' . "<td>$selectedTableRec[14]</td>" . '</span>'; //room number where the class takes place
            print '<span class="instructor">' . "<td>$selectedTableRec[15]</td>" . '</span>'; //who teaches the class
            print '<span class="netId">' . "<td>$selectedTableRec[16]</td>". '</span>'; //net id of instructor
            print '<span class="email">' ."<td>$selectedTableRec[17]</td>". '</span>'; //email address of the instructor
            print "\n\t</tr>";
        }
        elseif($selectedTableRec[3] == $csClass And $selectedTableRec!='') {
            print"\n<tr>";
            print '<option>' . '<span class="subj">' . "<td>$selectedTableRec[0]</td>". '</span>' . '&nbsp;';
            print '<span class="classNum">' ."<td>$selectedTableRec[1]</td>". '</span>' . '&nbsp;';
            print '<span class="title">' . "<td>$selectedTableRec[2]</td>" . '</span>';
            print '<span class="compNum">' . "<td>$selectedTableRec[3]</td>" . '</span>';
            print '<span class="section">' . "<td>$selectedTableRec[4]</td>". '</span>'. '</option>';
            print '<span class="lecLab">' ."<td>$selectedTableRec[5]</td>". '</span>';
            print '<span class="campCode">' . "<td>$selectedTableRec[6]</td>" . '</span>';
            print '<span class="maxEnroll">' . "<td>$selectedTableRec[7]</td>" . '</span>';
            print '<span class="curEnroll">' . "<td>$selectedTableRec[8]</td>". '</span>';
            print '<span class="startTime">' ."<td>$selectedTableRec[9]</td>". '</span>';
            print '<span class="endTime">' . "<td>$selectedTableRec[10]</td>" . '</span>';
            print '<span class="days">' . "<td>$selectedTableRec[11]</td>" . '</span>';
            print '<span class="credits">' . "<td>$selectedTableRec[12]</td>". '</span>';
            print '<span class="building">' ."<td>$selectedTableRec[13]</td>". '</span>';
            print '<span class="room">' . "<td>$selectedTableRec[14]</td>" . '</span>';
            print '<span class="instructor">' . "<td>$selectedTableRec[15]</td>" . '</span>';
            print '<span class="netId">' . "<td>$selectedTableRec[16]</td>". '</span>';
            print '<span class="email">' ."<td>$selectedTableRec[17]</td>". '</span>';
            print "\n\t</tr>";
        }
    }
    print '</table>';?>
    <style type="text/css"
        table{
        "border-spacing:20px"

        <?php include ("footer.php"); ?>

</html>


