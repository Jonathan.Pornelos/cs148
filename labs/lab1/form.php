<?php
include "top.php";
?>

    <h1>Selected Class</h1>
    
    <?php $myFileName = "curr_enroll_fall";

    $fileExt = ".csv";

    $filename = $myFileName . $fileExt;

    $file = fopen($filename, "r");

    /* This reads the first row, which in our case is the column headers */
    $headers = fgetcsv($file);
    
    /* the while (similar to a for loop) loop keeps executing until we reach 
     * the end of the file at which point it stops. the resulting variable 
     * $records is an array with all our data. */

    while (!feof($file)) {
        $selectedRecords[] = fgetcsv($file);
    }
//closes the file
    fclose($file);
    
    print"<table>";
    foreach ($headers as $oneHeader) {
        print"<th>$oneHeader</th>";
        print "\n";
    }
 
     foreach ($selectedRecords as $selectedTableRec) {
        if ($selectedTableRec[3] == $class And $selectedTableRec!='') {
            print"\n<tr>";
            print '<option>' . '<span class="subj">' . "<td>$selectedTableRec[0]</td>". '</span>' . '&nbsp;';
            print '<span class="classNum">' ."<td>$selectedTableRec[1]</td>". '</span>' . '&nbsp;';
            print '<span class="title">' . "<td>$selectedTableRec[2]</td>" . '</span>';
            print '<span class="compNum">' . "<td>$selectedTableRec[3]</td>" . '</span>';
            print '<span class="section">' . "<td>$selectedTableRec[4]</td>". '</span>'. '</option>';
            print '<span class="lecLab">' ."<td>$selectedTableRec[5]</td>". '</span>';
            print '<span class="campCode">' . "<td>$selectedTableRec[6]</td>" . '</span>';
            print '<span class="maxEnroll">' . "<td>$selectedTableRec[7]</td>" . '</span>';
            print '<span class="curEnroll">' . "<td>$selectedTableRec[8]</td>". '</span>';
            print '<span class="startTime">' ."<td>$selectedTableRec[9]</td>". '</span>';
            print '<span class="endTime">' . "<td>$selectedTableRec[10]</td>" . '</span>';
            print '<span class="days">' . "<td>$selectedTableRec[11]</td>" . '</span>';
            print '<span class="credits">' . "<td>$selectedTableRec[12]</td>". '</span>';
            print '<span class="building">' ."<td>$selectedTableRec[13]</td>". '</span>';
            print '<span class="room">' . "<td>$selectedTableRec[14]</td>" . '</span>';
            print '<span class="instructor">' . "<td>$selectedTableRec[15]</td>" . '</span>';
            print '<span class="netId">' . "<td>$selectedTableRec[16]</td>". '</span>';
            print '<span class="email">' ."<td>$selectedTableRec[17]</td>". '</span>';
            print "\n\t</tr>";
        }
    }
    print '</table>';?>

        <?php include ("footer.php"); ?>
</html>


