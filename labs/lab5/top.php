<?php
include "lib/constants.php";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>UVM Course Database | CS 148</title>
        <meta charset="utf-8">
        <meta name="author" content="Jonathan Pornelos">
        <meta name="description" content="This website displays and populates the tables for my UVM Course Database.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/sin/trunk/html5.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="css/base.css" type="text/css" media="screen">
        <?php
        // %^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%
        //
        // inlcude all libraries. Note some are in lib and some are in bin
        // bin should be located at the same level as www-root (it is not in 
        // github)
        // 
        // yourusername
        //     bin
        //     www-logs
        //     www-root
        //require_once($includeLibPath . 'mailMessage.php');
        //require_once('lib/security.php');
        print "<!-- require Database.php -->";
        include 'lib/validation.php';
        require_once(LIB_PATH . '/Database.php');
        // %^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%^%
         print "<!-- make Database connections -->";
         
            $domain= "//";
        $server=htmlentities($_SERVER['SERVER_NAME'], ENT_QUOTES, "UTF-8");
        $domain .= $server;
        $phpSelf = htmlentities($_SERVER['PHP_SELF'], ENT_QUOTES, "UTF-8");
        $path_parts = pathinfo($phpSelf);
        $yourURL=$domain.$phpSelf;
         
        $dbUserName = get_current_user() . '_reader';
        $whichPass = "r"; //flag for which one to use.
        $dbName = DATABASE_NAME;

        $thisDatabaseReader = new Database($dbUserName, $whichPass, $dbName);

        $dbUserName = get_current_user() . '_writer';
        $whichPass = "w";
        $thisDatabaseWriter = new Database($dbUserName, $whichPass, $dbName);

        

        $dbUserName = get_current_user() . '_admin';
        $whichPass = "a";
        $thisDatabaseAdmin = new Database($dbUserName, $whichPass, $dbName);
        $username=htmlentities($_SERVER["REMOTE_USER"],ENT_QUOTES,"UTF-8");
        $name =  ldapName($username);
        function ldapName($uvmID) {
            if (empty($uvmID))
                return "no:netid";
            $name = "not:found";
            
            $ds = ldap_connect("ldap.uvm.edu");

            if ($ds) {
                $r = ldap_bind($ds);
                $dn = "uid=$uvmID,ou=People,dc=uvm,dc=edu";
                $filter = "(|(netid=$uvmID))";
                $findthese = array("sn", "givenname");

                // now do the search and get the results which are stored in $info
                $sr = ldap_search($ds, $dn, $filter, $findthese);

                // if we found a match (in this example we should actually always find just one
                if (ldap_count_entries($ds, $sr) > 0) {
                    $info = ldap_get_entries($ds, $sr);
                    $name = $info[0]["givenname"][0] . ":" . $info[0]["sn"][0];
                }
                     }

                ldap_close($ds);

            return $name;
    
    
        }
$fullName=explode(":", $name);
    $firstName=$fullName[0];
    $lastName=$fullName[1];
?>
        
    </head>
    <!-- **********************     Body section      ********************** -->
    <?php
    print '<body id="' . $PATH_PARTS['filename'] . '">';
    include "header.php";
    include "nav.php";
    ?>