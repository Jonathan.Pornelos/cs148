<!-- ######################     Main Navigation   ########################## -->
<nav>
    <ol>
        <?php
        // This sets the current page to not be a link. Repeat this if block for
        //  each menu item 
        if ($PATH_PARTS['filename'] == "index") {
            print '<li class="activePage">Home</li>';
        } else {
            print '<li><a href="index.php">Home</a></li>';
        }
        if ($PATH_PARTS['filename'] == "dictionary") {
            print '<li class="activePage">Display Tables</li>';
        } else {
            print '<li><a href="dictionary.php">Display Tables</a></li>';
        }
        
        if ($PATH_PARTS['filename'] == "populate-table.php") {
            print '<li class="activePage">Populate Tables</li>';
        } else {
            print '<li><a href="populate-table.php">Populate Tables</a></li>';
        }
        if ($PATH_PARTS['filename'] == "form.php") {
            print '<li class="activePage">Form</li>';
        } else {
            print '<li><a href="form.php">Form</a></li>';
        }
        if($PATH_PARTS['filename'] == "fourYearTables.php") {
            print '<li class="activePage">Four Year Tables</li>';
        } else {
            print '<li><a href="fourYearTables.php">Four Year Tables</a></li>';
        }
        if($PATH_PARTS['filename'] == "semesterForm.php") {
            print '<li class="activePage">Semester Form</li>';
        } else {
            print '<li><a href="semesterForm.php">Semester Form</a></li>';
        }
        ?>
    </ol>
</nav>
<!-- #################### Ends Main Navigation    ########################## -->
