CREATE TABLE IF NOT EXISTS tblAdvisors(
	pmkNetId varchar(12) NOT NULL,
	fldEmail varchar(255) DEFAULT NULL,
	fldFirstName varchar(255) DEFAULT NULL,
	fldLastName varchar(255) DEFAULT NULL,
	PRIMARY KEY(pmkNetId));

CREATE TABLE IF NOT EXISTS tblStudents(
	pmkNetId varchar(12) NOT NULL,
	fldEmail varchar(255) DEFAULT NULL,
	fldFirstName varchar(255) DEFAULT NULL,
	fldLastName varchar(255) DEFAULT NULL,
	fldYearEnrolled int(4) unsigned DEFAULT NULL,
	PRIMARY KEY(pmkNetId));	
	
CREATE TABLE IF NOT EXISTS tblFourYearPlans(
	pmkPlanId int(11) NOT NULL AUTO_INCREMENT,
	fldDegree varchar(50) DEFAULT NULL,
	fldDateCreated timestamp DEFAULT NOW(),
	fldCatalogYear int(4) DEFAULT NULL,
	fldTotalCredits int(4) DEFAULT NULL,
	fnkStudentNetId varchar(12) NOT NULL,
	fnkAdvisorNetId varchar(12) NOT NULL,
	PRIMARY KEY(pmkPlanId));
	
CREATE TABLE IF NOT EXISTS tblSemesters(
	fnkPlanId int(11) NOT NULL,
	fldTermYear char(9) NOT NULL,
	fldTerm varchar(6) NOT NULL,
	fldTotalCredits int(4) DEFAULT NULL,
	PRIMARY KEY(fnkPlanId, fldTermYear, fldTerm));
	
CREATE TABLE IF NOT EXISTS tblCourses(
	pmkCourseId int(11) NOT NULL,
	fldCourseNumber int(11) NOT NULL,
	fldDepartment varchar(5) NOT NULL,
	fldCredits int(4) NOT NULL DEFAULT '3',
	PRIMARY KEY(pmkCourseId));
	
CREATE TABLE IF NOT EXISTS tblSemesterCourses(
	fnkPlanId int(11) NOT NULL,
	fnkCourseId int(11) NOT NULL,
	fnkTermYear char(9) NOT NULL,
	fnkTerm varchar(6) NOT NULL,
	fldRequirement varchar(50) DEFAULT NULL,
	PRIMARY KEY(fnkPlanId, fnkTermYear, fnkTerm, fnkCourseId));
	







