<?php   
        include 'top.php';
        $class="";
        $term="Fall";
        $termYear="";
        $course="";
        
        $query="SELECT pmkCourseId, fldCourseNumber,fldDepartment,fldCredits FROM tblCourses";
        $results=$thisDatabaseWriter->select($query,"",0,0,0,0,false,false);
        if(isset($_POST['2ndbtnSubmit'])){
//        foreach($results as $c){
//                #if($c[0]!=""){     
//                #$select='<select name="select">';
//                print "\n\t\t\t"."<option";
//                    //if($class==$c){
//                    //    print 'selected ';
//                    //}
//                    print 'value="' .$c[0]. '">' . $c[1] . '&nbsp;';
//                    print  $c[2] . "</option>";
//                    print "\n";
//                }
        $term = htmlentities($_POST["radTerm"],ENT_QUOTES,"UTF-8");
        $termYear = htmlentities($_POST["lstSemesterYear"],ENT_QUOTES,"UTF-8");
        $course=  htmlentities($_POST["lstCourse"],ENT_QUOTES,"UTF-8");
        session_start();
        $fnkPlanId=$_SESSION['pmkPlanId'];
    $query="INSERT IGNORE INTO tblSemesters(fnkPlanId, fldTermYear,fldTerm) VALUES(?,?,?)";
    $data=array($fnkPlanId,$termYear,$term);
    $results1=$thisDatabaseWriter->insert($query,$data,0,0,0,0,false,false);
    #if (isset($_POST[""]))
    #if (isset($_POST["2ndbtnSubmit"])) { // closing marked with 'end body submit'
    $query="INSERT IGNORE INTO tblSemesterCourses(fnkPlanId,fnkCourseId,fnkTermYear,fnkTerm) VALUES(?,?,?,?)";
    $data=array($fnkPlanId,$course,$termYear,$term);
    $results1=$thisDatabaseWriter->insert($query,$data,0,0,0,0,false,false);
    print "<h2>Your request has ";
    print "been processed.</h2>";
    #include 'semesterForm.php';
    #}
}
        ?>
<form action="<?php print $phpSelf; ?>"
              method="post"

              id="frmCreateSemesterPlan">


            <fieldset class="wrapper">
                <legend></legend>
                <p>Please provide the following information about yourself or your student.</p>
                <fieldset class="student-info">
                    <legend>Student Information</legend>
                    <fieldset class="listbox3">
                        <label for="lstSemesterYear">Semester Term Year</label>
                        <select id="lstSemesterYear" name="lstSemesterYear"
                                tabIndex="400">
                            <?php
                            // Array for listbox options
                            $list2Choices = array("2012", "2013", "2014", "2015","2016","2017","2018");
                            foreach ($list2Choices as $option) {
                                print "\n\t\t\t" . "<option ";
                                if($termYear==$option){print" selected";}
                                print 'value="' . $option . '">' . $option . "</option>";
                                print "\n";
                            }
                            ?>
                        </select>
                    </fieldset> <!-- end listbox3 -->
                    <fieldset class="radBtn">
                        <label>Choose Term</label>
                        <input type="radio" class="radTerm" name="radTerm" value="Fall"
                               <?php if($term=="Fall") print 'checked'?>>
                        Fall
                        <input type="radio" class="radTerm" name="radTerm" value="Spring"
                               <?php if($term=="Spring") print 'checked'?>>
                        Spring
                    </fieldset>
                        
                    
                    <fieldset class="listbox1">
                        <label for="lstCourse">Course List</label>
                        <select id="lstCourse" name="lstCourse"
                                tabIndex="420">
                        <?php
                        // Array for listbox options
                        foreach($results as $c){
                                    #if($c[0]!=""){     
                                    #$select='<select name="select">';
                                    print "\n\t\t\t"."<option value=".$c[0]. '">' . $c[1] . '&nbsp' .$c[2] . "</option> \n";
                                    
                                    #}
                            }
    ?>
                    </select>
                    </fieldset> <!-- end listbox1 -->

                </fieldset> <!-- end student-info -->
                <fieldset class="buttons">
                    <legend></legend>
                    <input type="submit" id="2ndbtnSubmit" name="2ndbtnSubmit" value="Submit" tabindex="900" class="button">
                </fieldset> <!-- ends buttons -->
            </fieldset> <!-- end wrapper! -->
        </form> <!-- end form! -->