<?php

//##############################################################################
//
// main home page for the site 
// 
//##############################################################################
include "top.php";

// Begin output
?>
<div class="page">
<h2 class="about">About this Project</h2>
   
    
<p class="aboutContent">With so many people not having cars, I wanted to help UVM students find a way to get home when they need to</p>
<p class="aboutContent">My solution is to provide a ride-sharing site that helps students get connected easily.</p>
<p class="aboutContent">If you notice anything about the site that I should know about (e.g., inappropriate content, a feature that's not working), please let me know! I can be reached at <a href="mailto:jpornelo@uvm.edu">jpornelo@uvm.edu</a>.</p>

<h3> The Coder</h3>
<div class="aboutProfile">
    <div class="pic">
        <figure><img class="profile" src="images/profilePic.jpg"/>
            <figcaption><p>Jon Pornelos</p></figcaption></figure>
    </div>
    <div class="p">
        <p class="aboutContent">Jon, the sole coder of the website. He has an Associates of Science in Business and is working on his Bachelors of Science in Computer Science with a minor in Mathematics.</p>
    </div>
</div>
</div>
<?php
include "footer.php";
?>
