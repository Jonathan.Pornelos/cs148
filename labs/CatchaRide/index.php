<?php

//##############################################################################
//
// main home page for the site 
// 
//##############################################################################
include "top.php";

// Begin output
?>
<div class="page">
<h2 class="home">Welcome to UVM's CatchaRide</h2>
   
    
    <p class="content"> Have you wanted to go home for a weekend, but you don't have a car and your 
        parents or friends can't come and get you. Well now there is an easy and free way to find
        rides home.</p>
    <p class="content1"> Presenting CatchaRide</p>
    <p class="content"> CatchaRide is UVM's easy to use ride-sharing website. It is an easy and 
        free* way for students to get home for the weekend or for breaks, while meeting new people
        and making new friends.</p>
        
    <p class="content"> All you have to do is sign up and you are ready to get started. Do you need
        a ride home? Just fill out the <a href="getARide.php" class="linkP">GetaRide</a> form and a driver can match
        with you or look up drivers that are also going home for the weekend and request to ride with them.</p>
    <p class="content"> Do you have a car and go home on the weekends? Sign up to <a href="giveARide.php" class="linkP">
            GiveaRide</a> and help someone out, and who doesn't like company on a long drive home.</p>
    
                        
</div>
<?php
include "footer.php";
?>
