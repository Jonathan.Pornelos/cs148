<!-- ######################     Main Navigation   ########################## -->
<nav class="navbar-fixed-top">
    <div class="container">
        
        <?php
        // This sets the current page to not be a link. Repeat this if block for
        //  each menu item 
        if ($PATH_PARTS['filename'] == "index") {
            print '<div class="navbar-header activePage"><a class="navbar-brand" href="index.php">CatchaRide</a></div>';
        } else {
            print '<div class="navbar-header"><a class="navbar-brand" href="index.php">CatchaRide</a></div>';
        }
        ?>
        <div id="navbar" class='navbar-left'>
            <ul class="nav navbar-nav">
                <?php
                if ($PATH_PARTS['filename'] == "giveARide" OR $PATH_PARTS['filename'] == "displayRiders") {
                    print '<li class="activePage"><a href="displayRiders.php">Give A Ride</a></li>';
                } else {
                    print '<li><a href="displayRiders.php">Give A Ride</a></li>';
                }
                if ($PATH_PARTS['filename'] == "getARide" OR $PATH_PARTS['filename'] == "displayDrivers") {
                    print '<li class="activePage"><a href="displayDrivers.php">Get A Ride</a></li>';
                } else {
                    print '<li><a href="displayDrivers.php">Get A Ride</a></li>';
                }
                #if ($PATH_PARTS['filename'] == "task3") {
                #    print '<li class="activePage"><a href="task3.php">Task 3</a></li>';
                #} else {
                #    print '<li><a href="task3.php">Task 3</a></li>';
                #}
                ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php
                $query="SELECT fldApproved FROM tblUsers WHERE pmkNetId=?";
                $data=array($username);
                $results=$thisDatabaseReader->select($query,$data,1,0,0,0,false,false);
                if(empty($results) OR $results[0]['fldApproved']!='1'){
                    if ($PATH_PARTS['filename'] == "reviews") {
                        print '<li class="activePage"><a href="task3.php">Reviews</a></li>';
                    } else {
                        print '<li><a href="task3.php">Reviews</a></li>';
                    }
                    if ($PATH_PARTS['filename'] == "signUp") {
                        print '<li class="activePage"><a href="signUp.php">Sign Up</a></li>';
                    } else {
                        print '<li><a href="signUp.php">Sign Up</a></li>';
                    }
                    if ($PATH_PARTS['filename'] == "about") {
                        print '<li class="activePage"><a href="about.php">About</a></li>';
                    } else {
                        print '<li><a href="about.php">About</a></li>';
                    }
                }
                else{
                    if ($PATH_PARTS['filename'] == "task3" OR $PATH_PARTS['filename']== "reviews") {
                        print '<li class="activePage"><a href="task3.php">Reviews</a></li>';
                    } else {
                        print '<li><a href="task3.php">Reviews</a></li>';
                    }
                    if ($PATH_PARTS['filename'] == "user") {
                        print '<li class="activePage"><a href="user.php">'.$username.'</a></li>';
                    } else {
                        print '<li><a href="user.php">'.$username.'</a></li>';
                    }
                    if ($PATH_PARTS['filename'] == "about") {
                        print '<li class="activePage"><a href="about.php">About</a></li>';
                    } else {
                        print '<li><a href="about.php">About</a></li>';
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<?php if($adminStatus==TRUE){ ?>
<nav class="nav-fixed-side">
    <div class='nav-bottom'>
        <h6 class="nav-bottom">Admin Nav</h6>
        <ul class="bottom">
                <?php
                if ($PATH_PARTS['filename'] == "userPosts") {
                    print '<li class="bottom activePage"><a href="userPosts.php">User Posts</a></li>';
                } else {
                    print '<li class="bottom"><a href="userPosts.php">User Posts</a></li>';
                }
                if ($PATH_PARTS['filename'] == "userReviews") {
                    print '<li class="bottom activePage"><a href="userReviews.php">Pending Reviews</a></li>';
                } else {
                    print '<li class="bottom"><a href="userReviews.php">Pending Reviews</a></li>';
                }
                #if ($PATH_PARTS['filename'] == "task3") {
                #    print '<li class="activePage"><a href="task3.php">Task 3</a></li>';
                #} else {
                #    print '<li><a href="task3.php">Task 3</a></li>';
                #}
                ?>
            </ul>
    </div>
</nav>
<?php } ?>
