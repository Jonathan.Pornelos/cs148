<!-- ######################     Main Navigation   ########################## -->
<nav>
    <ol>
        <?php
        // This sets the current page to not be a link. Repeat this if block for
        //  each menu item 
        if ($PATH_PARTS['filename'] == "index") {
            print '<li class="activePage">Home</li>';
        } else {
            print '<li><a href="index.php">Home</a></li>';
        }
        if ($PATH_PARTS['filename'] == "task1") {
            print '<li class="activePage"><a href="task1.php">Task 1</a></li>';
        } else {
            print '<li><a href="task1.php">Task 1</a></li>';
        }
        if ($PATH_PARTS['filename'] == "task2") {
            print '<li class="activePage"><a href="task2.php">Task 2</a></li>';
        } else {
            print '<li><a href="task2.php">Task 2</a></li>';
        }
        if ($PATH_PARTS['filename'] == "task3") {
            print '<li class="activePage"><a href="task3.php">Task 3</a></li>';
        } else {
            print '<li><a href="task3.php">Task 3</a></li>';
        }
        ?>
    </ol>
</nav>
<!-- #################### Ends Main Navigation    ########################## -->
