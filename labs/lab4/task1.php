<?php
include	"top.php";
//########################################################
// This page lists the records based on	the query given
//##########################################
$query = 'SELECT fldFirstName, fldLastName FROM tblTeachers ORDER BY fldLastName, fldFirstName';


//public function select($query, $values = "", $wheres = 1, $conditions	= 0,
//$quotes = 0, $symbols = 0, $spacesAllowed = false, $semiColonAllowed = false)
$assign4 = $thisDatabaseReader->select($query, "", 0, 1, 0, 0, false, false);
$highlight = 0;
if (DEBUG) {
    print "<p>Contents of the array<pre>";
    print_r($assign4);
    print "</pre></p>";
}	
print'<h2 class="alternateRows">Task 1</h2>';
if (is_array($assign4)) {
    print "<table class = 'assignment4'>";
    print '<tr class = "heading"><th>First Name</th><th>Last Name</th></tr>';
    foreach ($assign4 as $record) {
        $highlight++;
        if ($highlight % 2 != 0) {
            $style = ' odd ';
        } else {
            $style = ' even ';
        }
        print '<tr class="' . $style . '">';
        print "<td class = 'thf'>" . $record['fldFirstName'] ."</td>";
        print "<td class = 'thf'>" . $record['fldLastName'] . "</td>";
        print "\n\t</tr>";
        }
        print '</table>';
}	
include "footer.php";
?>	