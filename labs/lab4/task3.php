<?php
include	"top.php";
//########################################################
// This page lists the records based on	the query given
//##########################################
//$query = "SELECT fldFirstName, fldLastName FROM tblPeople WHERE fldDepartment = ? ORDER BY fldLastName, fldFirstName";
$query = "SELECT fldFirstName, fldLastName, SUM(fldNumStudents) as NUM FROM tblStudents WHERE fldDepartment = ? GROUP BY fldFirstName, fldLastName ORDER BY NUM DESC";
$data = array('CS');
//public function select($query, $values = "", $wheres = 1, $conditions	= 0,
//$quotes = 0, $symbols = 0, $spacesAllowed = false, $semiColonAllowed = false)
$assign6 = $thisDatabaseReader->select($query, $data, 1, 1, 0, 0, false, false);
$highlight = 0;
if (DEBUG) {
    print "<p>Contents of the array<pre>";
    print_r($assign6);
    print "</pre></p>";
}	
print'<h2 class="alternateRows">Task 3</h2>';
if (is_array($assign6)) {
    print "<table class = 'assignment5'>";
    print '<tr class = "heading"><th>First Name</th><th>Last Name</th><th>Number Of Students</th></tr>';
    foreach ($assign6 as $record) {
        $highlight++;
        if ($highlight % 2 != 0) {
            $style = ' odd ';
        } else {
            $style = ' even ';
        }
        print '<tr class="' . $style . '">';
        print "<td class = 'thf'>" . $record['fldFirstName'] ."</td>";
        print "<td class = 'thf'>" . $record['fldLastName'] . "</td>";
        print "<td class = 'thf'>" . $record[2] . "</td>";
        print "\n\t</tr>";
        }
        print '</table>';
}	
include "footer.php";