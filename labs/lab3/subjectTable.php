<?php

$file2 = fopen($filename2, 'r');
$file3 = fopen($filename, 'r');
/* This reads the first row, which in our case is the column headers */

$headers2 = fgetcsv($file2);
$headers3 = fgetcsv($file3);
/* the while (similar to a for loop) loop keeps executing until we reach 
 * the end of the file at which point it stops. the resulting variable 
 * $records is an array with all our data. */

while (!feof($file2)) {
    $selectedRecords2[] = fgetcsv($file2);
}
while (!feof($file3)) {
    $selectedRecords3[] = fgetcsv($file3);
}
//closes the file
//fclose($file);
fclose($file2);
fclose($file3);
//    Creates a table of all of the data of the class that is selected with headers
print'<table class="one">';
if ($subClass == "") {
    print "<tr>";
    foreach ($headers3 as $Header3) {
        //if ($header2!='0'){
        
        if ($Header3 != '0') {
            print"<th>$Header3</th>";
            
        }
        
        $highlight = 0;
    }print"</tr>";
    foreach ($selectedRecords3 as $selected) {
        $highlight++;
        if ($highlight % 2 != 0) {
            $style = ' odd ';
        } else {
            $style = ' even ';
        }
        print '<tr class="' . $style . '">';
        print "<td>$selected[0]</td>" ;
        print "<td>$selected[1]</td>";
        print "<td>$selected[2]</td>";
        print "<td>$selected[3]</td>";
        print "<td>$selected[4]</td>"; //class section
        print "<td>$selected[5]</td>"; //class lecture lab
        print "<td>$selected[6]</td>"; //campus code
        print "<td>$selected[7]</td>"; //max enrollment limit
        print "<td>$selected[8]</td>"; //current amount enrolled in the class
        print "<td>$selected[9]</td>"; //start time of the class
        print "<td>$selected[10]</td>"; //end time of the class
        print "<td>$selected[11]</td>"; //what days the class takes place
        print "<td>$selected[12]</td>"; //how many credits the course is
        print "<td>$selected[13]</td>"; //what building the class takes place
        print "<td>$selected[14]</td>"; //room number where the class takes place
        print "<td>$selected[15]</td>"; //who teaches the class
        print "<td>$selected[16]</td>"; //net id of instructor
        print "<td>$selected[17]</td>"; //email address of the instructor
        print "\n\t</tr>";
    }
    print '</table>';
} else {
    print'<table class="two">';
    foreach ($headers2 as $header2) {
        if ($header2!='Course_Id'){
        print"<th>$header2</th>";
        print "\n";
        //}
    }
    }
    $highlight = 0;
    foreach ($selectedRecords2 as $selectedCourseRec) {
        if ($selectedCourseRec[3] == $subClass And $selectedCourseRec != '') {
            $highlight++;
            if ($highlight % 2 != 0) {
                $style = ' odd ';
            } else {
                $style = ' even ';
            }
            print '<tr class="' . $style . '">';
            print "<td>$selectedCourseRec[0]</td>" ;
            print "<td>$selectedCourseRec[1]</td>";
            print "<td>$selectedCourseRec[2]</td>";
            #print "<td>$selectedCourseRec[3]</td>";

            print "\n\t</tr>";
        }
    }
    print'</table>';
}
?>