<?php
$file = fopen($filename, 'r');
/* This reads the first row, which in our case is the column headers */
$headers = fgetcsv($file);

/* the while (similar to a for loop) loop keeps executing until we reach 
 * the end of the file at which point it stops. the resulting variable 
 * $records is an array with all our data. */

while (!feof($file)) {
    $selectedRecords[] = fgetcsv($file);
}

//closes the file
fclose($file);

//    Creates a table of all of the data of the class that is selected with headers
print'<table class="info">';
print "<tr>";
foreach ($headers as $oneHeader) {
    if($oneHeader!='0'){
        print"<th>$oneHeader</th>";
        print "\n";
    }
}
print "</tr>";
$highlight = 0;
foreach ($selectedRecords as $selectedTableRec) {
    if($selectedTableRec[3] == $class And $selectedTableRec != '') {
        $highlight++;
        if ($highlight % 2 != 0) {
            $style = ' odd ';
        } else {
            $style = ' even ';
        }
        print '<tr class="'.$style.'">';
        print "<td>$selectedTableRec[0]</td>"; //class subject
        print "<td>$selectedTableRec[1]</td>"; //class number
        print "<td>$selectedTableRec[2]</td>" ; //class title
        print "<td>$selectedTableRec[3]</td>" ; //class comp number
        print "<td>$selectedTableRec[4]</td>" ; //class section
        print "<td>$selectedTableRec[5]</td>" ; //class lecture lab
        print "<td>$selectedTableRec[6]</td>" ; //campus code
        print "<td>$selectedTableRec[7]</td>" ; //max enrollment limit
        print "<td>$selectedTableRec[8]</td>" ; //current amount enrolled in the class
        print "<td>$selectedTableRec[9]</td>" ; //start time of the class
        print "<td>$selectedTableRec[10]</td>" ; //end time of the class
        print "<td>$selectedTableRec[11]</td>" ; //what days the class takes place
        print "<td>$selectedTableRec[12]</td>" ; //how many credits the course is
        print "<td>$selectedTableRec[13]</td>" ; //what building the class takes place
        print "<td>$selectedTableRec[14]</td>" ; //room number where the class takes place
        print "<td>$selectedTableRec[15]</td>" ; //who teaches the class
        print "<td>$selectedTableRec[16]</td>" ; //net id of instructor
        print "<td>$selectedTableRec[17]</td>" ; //email address of the instructor
        print "\n\t</tr>"; 
    }
}

print '</table>';
?>