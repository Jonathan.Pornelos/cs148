<?php
include "top.php";
//##############################################################################
//
// This page is designed to show how to insert a record. It does not actually
// insert a record since that line is commentted out. It does show the results
// the testquery method so you can see what that looks like.
//
//##############################################################################
// This example does not have a form so these two lines are just to put values
// in manually for this example.
$file = fopen($filename, 'r');
/* This reads the first row, which in our case is the column headers */
$headers = fgetcsv($file);

/* the while (similar to a for loop) loop keeps executing until we reach 
 * the end of the file at which point it stops. the resulting variable 
 * $records is an array with all our data. */

while (!feof($file)) {
    $selectedQueryRecords[] = fgetcsv($file);
}

//closes the file
fclose($file);
$b="";
$pmkCourseId = 0;
$subj = "";
$num = 0;
$title = "";
foreach ($selectedQueryRecords as $queryRecords){
//    
    $query = "INSERT INTO tblCourses(pmkCourseId, fldCourseNumber, fldCourseName, fldDepartment, fldCredits) ";
    $query .= "VALUES (?, ?, ?, ?, ?)";
    $data = array($queryRecords[19], $queryRecords[1], $queryRecords[2], $queryRecords[0], $queryRecords[12]);
    if (!($subj == $queryRecords[0] and $num == $queryRecords[1] and $title == $queryRecords[2])) {
        $results = $thisDatabaseWriter->insert($query, $data);
        $pmkCourseId = $thisDatabaseWriter->lastInsert();
       
    }
    $subj = $queryRecords[0];
    $num = $queryRecords[1];
    $title = $queryRecords[2];
    
    if($queryRecords[15]!="" and $queryRecords[16]!= $b){
        $query = "INSERT INTO tblTeachers(fldLastName, fldFirstName, pmkNetId, fldEmail) ";
        $query .= "VALUES (?, ?, ?, ?)";
        $data = explode (', ', $queryRecords[15]);
        $data[] = $queryRecords[16];
        $data[] = $queryRecords[17];
        $results = $thisDatabaseWriter->insert($query, $data, 0, 0, 0, 0, false, false);
        $b = $queryRecords[16];
    }
    
    $query = "INSERT INTO tblSections(fnkCourseId, fldCRN, fnkTeacherNetId, fldMaxStudents, fldNumStudents, fldSection, fldType, fldStart, fldStop, fldDays, fldBuilding, fldRoom) ";
    $query .= "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $data = array($queryRecords[18], $queryRecords[3], $queryRecords[16], $queryRecords[7], $queryRecords[8], $queryRecords[4], $queryRecords[5], $queryRecords[9], $queryRecords[10], $queryRecords[11], $queryRecords[13], $queryRecords[14]);
    $results = $thisDatabaseWriter->insert($query, $data);
}

// demostration of test query method which returns nothing but displays information.
print "<h2>TEST Query method</h2>";
$records = $thisDatabaseReader->testquery($query, $data, 0, 0, 0, 0, false, false);

// this will insert the data, since i dont want Mr. Spacely entered into my table
// again i commented this line out. $records will always be false because of that.
print "<h2>Insert method</h2>";
//$records = $thisDatabaseWriter->insert($query, $data, 0, 0, 0, 0, false, false);


if ($results) {
    print "<p>Record Saved</p>";
} else {
    print "<p>Record NOT Saved</p>";
}

include "footer.php";
?>