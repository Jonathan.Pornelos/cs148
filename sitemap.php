<!DOCTYPE html>
<HTML lang="en">

    <head>
        <title>CS 148 Assignments</title>
        <meta charset="utf-8">
        <meta name="author" content="Jonathan P Pornelos">
        <meta name="description" content="This is my site map for my CS 148 assignments.">
        <style type="text/css">
            img{
                width: 100%;
            }
        </style>
    </head>

    <SPAN><img src="banner.png" class="banner" /></SPAN>
            
    <h1>Jonathan Pornelos</h1>
    
    <p>CatchaRide:</p>
    <ol>
<!--        <li><a href="labs/lab5/index.php">Home</a></li>-->
        <li><a href="labs/CatchaRide/index.php">Home</a></li>
        <li><a href="labs/CatchaRide/getARide.php">Get A Ride</a></li>
        <li><a href="labs/CatchaRide/giveARide.php">Give a Ride</a></li>
        <li><a href="labs/CatchaRide/dictionary.php">Data Dictionary</a></li>
        <li><a href="labs/CatchaRide/ErDiagram.pdf">ER Diagram</a></li>
        <li><a href="labs/CatchaRide/schema.pdf">Schema</a></li>
        <li><a href="labs/CatchaRide/images/storyBoard.jpg">Story Board</a></li>
        <li><a href="labs/CatchaRide/base.css">Style</a></li>
       
    </ol>
    
    <p>Assignment 5:</p>
    <ol>
<!--        <li><a href="labs/lab5/index.php">Home</a></li>-->
        <li><a href="labs/lab5/index.php">Home</a></li>
        <li><a href="labs/lab5/form.php">Form</a></li>
        <li><a href="labs/lab5/populate-table.php">Populate Table</a></li>
        <li><a href="labs/lab5/dictionary.php">Data Dictionary</a></li>
        <li><a href="labs/lab5/ErDiagram1.pdf">ER Diagram</a></li>
        <li><a href="labs/lab5/schema1.pdf">Schema</a></li>
        <li><a href="labs/lab5/css/base.css">Style</a></li>
       
    </ol>
    
    <p>Assignment 4:</p>
    <ol>
        <li><a href="labs/lab4/index.php">Home</a></li>
        <li><a href="labs/lab4/css/base.css">Style</a></li>
       
    </ol>
    
    
    <p>Assignment 3:</p>
    <ol>
        <li><a href="labs/lab3/index.php">Home</a></li>
        <li><a href="labs/lab3/tables.php">Data Dictionary</a></li>
        <li><a href="labs/lab3/er-diagram.jpg">ER-Diagram</a></li>
        <li><a href="labs/lab3/schema.html">Schema</a></li>
        <li><a href="labs/lab3/css/base.css">Style</a></li>
       
    </ol>
    
    <p>Assignment 2:</p>
    <ol>
        <li><a href="labs/lab2/index.php">Home</a></li>
        <li><a href="labs/lab2/css/base.css">Style</a></li>
       
    </ol>

   <p>Assignment 1:</p>
    <ol>
        <li><a href="labs/lab1/index.php">Home</a></li>
        <li><a href="labs/lab1/style.css">Creative</a></li>
       
    </ol>

</body>
</html>
